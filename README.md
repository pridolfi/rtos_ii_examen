**Sistemas Operativos de Tiempo Real II - 4B2015 - Examen Final 14/10/2015**

*Consigna:*

Implemente un sistema que mida la duración de 4 pulsos y envíe la medición a 
través de una UART con formato JSON:

`{canal:tiempo_ms}\r\n`

Donde el campo canal puede ser 0, 1, 2 o 3 y el campo tiempo_ms el tiempo medido
en milisegundos. Por ejemplo:

`{0:2746}\r\n`

El sistema debe ser capaz de medir los 4 canales en forma independiente ya 
que pueden ocurrir pulsos simultáneos de distinta duración.
* Utilice los 4 pulsadores de la EDU-CIAA para simular los pulsos (medir 
tiempo de pulsación y aplicar medidas para evitar ruido por rebote). El device 
correspondiente es `/dev/dio/in/0`
* Utilice la UART-USB para enviar la información. Recuerde evitar accesos 
concurrentes al periférico. El device correspondiente es `/dev/serial/uart/1`
* Destelle el LED correspondiente sobre el pulsador para obtener una 
indicación visual del pulso (`/dev/dio/out/0`).

Nota: Los drivers de las UARTs de CIAA-Firmware funcionan por interrupción. 
No olvide agregar en el archivo oil los ISRs correspondientes.