/* Copyright 2014, Mariano Cerdeiro
 * Copyright 2014, Pablo Ridolfi
 * Copyright 2014, Juan Cecconi
 * Copyright 2014, Gustavo Muro
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking_echo example source file
 **
 ** Examen RTOS II 4B2015
 ** https://docs.google.com/document/d/1FgdlM3K0yMWN-ALwCa7ukl4YbVQdP4vA2-WhTzla9_w/edit?usp=sharing
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Blinking Blinking_echo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * MaCe         Mariano Cerdeiro
 * PR           Pablo Ridolfi
 * JuCe         Juan Cecconi
 * GMuro        Gustavo Muro
 * ErPe         Eric Pernia
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20150603 v0.0.3   ErPe change uint8 type by uint8_t
 *                        in line 172
 * 20141019 v0.0.2   JuCe add printf in each task,
 *                        remove trailing spaces
 * 20140731 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/
#include "os.h"               /* <= operating system header */
#include "ciaaPOSIX_stdio.h"  /* <= device handler header */
#include "ciaaPOSIX_string.h" /* <= string header */

#include "ciaak.h"            /* <= ciaa kernel header */
#include "main.h"         /* <= own header */
#include "queue.h"

/*==================[macros and definitions]=================================*/

#define N_CHANNELS 4

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/** \brief File descriptor for digital output ports
 *
 * Device path /dev/dio/out/0
 */
static int32_t fd_out;

static int32_t fd_in;

static int32_t fd_uart;

static uint32_t tiempo_ms[N_CHANNELS];

static uint8_t LEDs[N_CHANNELS] = {0x01, 0x08, 0x10, 0x20};

static AlarmType LEDOffAlarm[N_CHANNELS] = { LEDOffAlarm0, LEDOffAlarm1, LEDOffAlarm2, LEDOffAlarm3};

static queue_t q;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

 /* reverse:  reverse string s in place */
 static void reverse(char s[])
 {
     int i, j;
     char c;
 
     for (i = 0, j = ciaaPOSIX_strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

 /* itoa:  convert n to characters in s */
 static void itoa(int n, char s[])
 {
     int i, sign;
 
     if ((sign = n) < 0)  /* record sign */
         n = -n;          /* make n positive */
     i = 0;
     do {       /* generate digits in reverse order */
         s[i++] = n % 10 + '0';   /* get next digit */
     } while ((n /= 10) > 0);     /* delete it */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }
 
/*==================[external functions definition]==========================*/
/** \brief Main function
 *
 * This is the main entry point of the software.
 *
 * \returns 0
 *
 * \remarks This function never returns. Return value is only to avoid compiler
 *          warnings or errors.
 */
int main(void)
{
   /* Starts the operating system in the Application Mode 1 */
   /* This example has only one Application Mode */
   StartOS(AppMode1);

   /* StartOs shall never returns, but to avoid compiler warnings or errors
    * 0 is returned */
   return 0;
}

/** \brief Error Hook function
 *
 * This fucntion is called from the os if an os interface (API) returns an
 * error. Is for debugging proposes. If called this function triggers a
 * ShutdownOs which ends in a while(1).
 *
 * The values:
 *    OSErrorGetServiceId
 *    OSErrorGetParam1
 *    OSErrorGetParam2
 *    OSErrorGetParam3
 *    OSErrorGetRet
 *
 * will provide you the interface, the input parameters and the returned value.
 * For more details see the OSEK specification:
 * http://portal.osek-vdx.org/files/pdf/specs/os223.pdf
 *
 */
void ErrorHook(void)
{
   ciaaPOSIX_printf("ErrorHook was called\n");
   ciaaPOSIX_printf("Service: %d, P1: %d, P2: %d, P3: %d, RET: %d\n", OSErrorGetServiceId(), OSErrorGetParam1(), OSErrorGetParam2(), OSErrorGetParam3(), OSErrorGetRet());
   ShutdownOS(0);
}

/** \brief Initial task
 *
 * This task is started automatically in the application mode 1.
 */
TASK(InitTask)
{
   /* init CIAA kernel and devices */
   ciaak_start();

   /* open CIAA digital outputs */
   fd_out = ciaaPOSIX_open("/dev/dio/out/0", ciaaPOSIX_O_RDWR);

   fd_in = ciaaPOSIX_open("/dev/dio/in/0", ciaaPOSIX_O_RDWR);

   fd_uart = ciaaPOSIX_open("/dev/serial/uart/1", ciaaPOSIX_O_RDWR);

   queueInit(&q, evQueue);

   ActivateTask(CommTask);
   ActivateTask(MeasureTask);

   /* terminate task */
   TerminateTask();
}

TASK(CommTask)
{
	queueItem_t d = queueGet(&q);
	char buffer[50];
	char str[50];

	buffer[0] = 0;
	str[0] = '{';
	str[1] = 0;

	itoa(d.ch, buffer);
	ciaaPOSIX_strcat(str, buffer);

	ciaaPOSIX_strcat(str, ":");

	itoa(d.time_ms, buffer);
	ciaaPOSIX_strcat(str, buffer);

	ciaaPOSIX_strcat(str, "}\r\n");

	ciaaPOSIX_write(fd_uart, str, ciaaPOSIX_strlen(str));

	ChainTask(CommTask);
}

TASK(MeasureTask)
{
	int32_t i;
	uint8_t inputs, outputs;
	static uint8_t state[N_CHANNELS];

	ciaaPOSIX_read(fd_in, &inputs, 1);

	for(i=0; i<N_CHANNELS; i++) {
		switch(state[i]) {
			case 0:
				if ((inputs & (1<<i)) == 0) {
					tiempo_ms[i] = 0;
					state[i] = 1;
				}
				else {
					state[i] = 0;
				}
				break;
			case 1:
				if (tiempo_ms[i] >= 20) {
					if ((inputs & (1<<i)) == 0) {
						state[i] = 2;
					}
					else {
						state[i] = 0;
					}
				}
				break;
			case 2:
				if ((inputs & (1<<i)) != 0) {
					queueItem_t d = {i, tiempo_ms[i]};
					queuePut(&q, d);

					ciaaPOSIX_read(fd_out, &outputs, 1);

					if((outputs & LEDs[i])==0)
					{
						outputs |= LEDs[i];
						ciaaPOSIX_write(fd_out, &outputs, 1);
						SetRelAlarm(LEDOffAlarm[i], tiempo_ms[i], 0);
					}

					state[i] = 0;
				}
				break;
			default:
				state[i] = 0;
				break;
		}
	}
	ChainTask(MeasureTask);
}

TASK(LEDOff0)
{
	uint8_t outputs;
	ciaaPOSIX_read(fd_out, &outputs, 1);
	outputs &= ~LEDs[0];
	ciaaPOSIX_write(fd_out, &outputs, 1);
	TerminateTask();
}
TASK(LEDOff1)
{
	uint8_t outputs;
	ciaaPOSIX_read(fd_out, &outputs, 1);
	outputs &= ~LEDs[1];
	ciaaPOSIX_write(fd_out, &outputs, 1);
	TerminateTask();
}
TASK(LEDOff2)
{
	uint8_t outputs;
	ciaaPOSIX_read(fd_out, &outputs, 1);
	outputs &= ~LEDs[2];
	ciaaPOSIX_write(fd_out, &outputs, 1);
	TerminateTask();
}
TASK(LEDOff3)
{
	uint8_t outputs;
	ciaaPOSIX_read(fd_out, &outputs, 1);
	outputs &= ~LEDs[3];
	ciaaPOSIX_write(fd_out, &outputs, 1);
	TerminateTask();
}

ALARMCALLBACK(TickHook)
{
	int32_t i;
	for(i=0; i<N_CHANNELS; i++) {
		tiempo_ms[i]++;
	}
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
